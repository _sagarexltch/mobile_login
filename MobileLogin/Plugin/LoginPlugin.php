<?php

namespace Excellence\MobileLogin\Plugin;

class LoginPlugin
{
    protected $customerFactory;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_customerFactory = $customerFactory;
        $this->request = $request;
    }
    public function beforeExecute()
    {
        $data = $this->request->getPostValue('login');
        $customer = $this->_customerFactory->create()->getCollection()
            ->addAttributeToSelect("*")
            ->addAttributeToFilter("mobile_number", array("eq" => $data['username']))
            ->load();

        if ($customer->getSize()) {
            $cust = $customer->getData();
            $data['username'] = $cust[0]['email'];
            $this->request->setPostValue('login', $data);
        }
    }
}
