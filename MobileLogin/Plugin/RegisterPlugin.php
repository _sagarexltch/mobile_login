<?php

namespace Excellence\MobileLogin\Plugin;

class RegisterPlugin
{
    protected $resultRedirectFactory;
    protected $customerFactory;
    private $messageManager;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        $this->_customerFactory = $customerFactory;
        $this->messageManager = $messageManager;
        $this->request = $request;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }
    public function beforeExecute()
    {
        $data = $this->request->getPost();
        $customer = $this->_customerFactory->create()->getCollection()
            ->addAttributeToSelect("*")
            ->addAttributeToFilter("mobile_number", array("eq" => $data['mobile_number']))
            ->load();
        if ($customer->getSize()) {
            return $this->resultRedirectFactory->create()->setPath('customer/account/create', ['_current' => true]);
            $this->messageManager->addError(__('This mobile number is already register with us.'));
        }
    }
}
